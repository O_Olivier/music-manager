exports.index = function(req, res) {
    const jsonObj = require('../public/music/music.json');
    res.render('index', { data: jsonObj });
};

// Display music create form on GET.
exports.music_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: music create GET');
};

// Display music delete form on GET.
exports.music_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: music delete GET');
};

// Display music update form on GET.
exports.music_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: music update GET');
};
