const trDefaultHeight = '30px';
const trBigHeight = '100px';

const artistTable = document.getElementById('mainTable');
const thead = artistTable.tHead;
const tbody = artistTable.tBodies[0];

//const addArtistForm = document.getElementById('addArtistForm');

let oldClickedRow = null;

function retrieve (artist, field) {
    if(artist[field] == undefined) return "";
    return artist[field];
}

function appendCell(row, artist, field) {
    const cell = row.insertCell();
    let text;
    if(field === 'website'){
	const url = retrieve(artist, field);
	if(url !== ""){
	    text = document.createElement('A');
	    text.href = url;
	    const urlParts = url.replace('http://','')
		  .replace('https://','')
		  .split(/[/?#]/);
	    const domain = urlParts[0];
	    text.innerHTML = domain;
		//"link to website (open in new tab)";
	    text.target="_blank";
	}else{
	    text = document.createTextNode(retrieve(artist, field));
	}
    }else{
	text = document.createTextNode(retrieve(artist, field));
    }
    cell.appendChild(text);
    row.appendChild(cell);
}

function artistFields() {
    const theadCells = Array.from(thead.rows[0].cells);
    const fields = theadCells.map(
	c => c.children[0].textContent.toLowerCase()
    );
    return fields;
}

function insertRow(artist, cssClass) {
    const row = tbody.insertRow();
    row.classList.add(cssClass);

    for(let field of artistFields()){
	appendCell(row, artist, field);
    }

    /*    
    row.addEventListener('click', () => {
	if(oldClickedRow != null)
	    oldClickedRow.style.height = trDefaultHeight;

	if(oldClickedRow === row)
	    row.style.height = trDefaultHeight;

	else
	    row.style.height = trBigHeight;
	
	oldClickedRow = row;
    });
    */
    
    return row;
}

function displayFirstListen(jsonObj) {
    const firstListenArtists = jsonObj['firstListen'];

    for(let artist of firstListenArtists)
	insertRow(artist, "firstListen");
}

function displaySecondListen(jsonObj) {
    const firstListenArtists = jsonObj['secondListen'];

    for(let artist of firstListenArtists)
	insertRow(artist, "secondListen");
}

function displayMusicTable(jsonObj){
    displayFirstListen(jsonObj);
    displaySecondListen(jsonObj);
}

function sort(field) {
    const index = artistFields().findIndex(f => f === field);
    const rows = Array.from(tbody.rows);
    const column = rows.map(r => {
	const ar = Array.from(r.cells);
	if(ar.length > index)
	    return ar[index].textContent;
	else
	    return undefined;
    });
    const columnMap = new Map();

    for(let i=0 ; i<column.length ; i++){
	const values = columnMap.get(column[i]);
	
	if(values == undefined){
	    columnMap.set(column[i], [rows[i]]);
	}else{
	    values.push(rows[i]);
	    columnMap.set(column[i], values);
	}
    }

    column.sort();

    for(let values of column)
	for(let value of columnMap.get(values))
	    tbody.appendChild(value);
}

function correctCase(s) {
    const strs = s.split(' ');
    const mappedStrs = strs.map(s => s.charAt(0).toUpperCase() + s.slice(1));
    return mappedStrs.join(' ');
}
/*
function handleAddArtistSubmit(event) {
    event.preventDefault();
    const input = document.getElementById('addArtistInput').value;
    const artistName = correctCase(input.trim());
    insertRow({"name": artistName});
}

addArtistForm.addEventListener('submit', handleAddArtistSubmit);
*/
