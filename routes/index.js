var express = require('express');
var router = express.Router();

const musicController = require('../controllers/musicController');

router.get('/', (req, res, next) => {
    musicController.index(req, res);
});

router.get('/music/create', (req, res, next) => {
    musicController.music_create_get(req, res);
});

router.get('/music/:id/delete', (req, res) => {
    musicController.music_delete_get(req, res);
});

router.get('/music/:id/update', (req, res) => {
    musicController.music_update_get(req, res);
});

module.exports = router;
