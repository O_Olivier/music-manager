build:
	npm install

run:
	npm run start

debug:
	DEBUG=music-manager:* npm run devstart

clean:
	rm -f *~ */*~
