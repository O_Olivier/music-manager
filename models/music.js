const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const musicSchema = new Schema(
    {
	_id: Schema.Types.ObjectId,
	artist_name: { type: String, required: true, max: 100 },
	genre: { type: String, required: false, max: 100 },
	country: { type: String, required: false, max: 100 },
    }
);

module.exports = mongoose.model('Music', musicSchema);
